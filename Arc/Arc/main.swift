import Foundation

protocol Toggle{
    
}

enum VolumeStorage : String{
    case empty = "пусто"
    case fill = "что-то есть"
}


enum TypeOfStorage : String{
    case trunk = "Багажник"
    case carCase = "Кузов"
}
enum load{
    case loadIn
    case loadOut
}

enum Engine : String, Toggle{

    case engineOn = "включен"
    case engineOff = "отключен"
    
    mutating func startStopEngine(){
        switch self{
        case .engineOn:
            self = .engineOff
        case .engineOff:
            self = .engineOn
        }
    }
}

enum Window : String, Toggle{

    case close = "закрыты"
    case open = "открыты"
    
    mutating func openCloseWindow() {
        switch self{
        case .close:
            self = .open
        case .open:
            self = .close
        }
    }
}

//*Описать несколько структур – любой легковой автомобиль и любой грузовик.
//*Структуры должны содержать марку авто, год выпуска, объем багажника/кузова, запущен ли двигатель, открыты ли окна, заполненный объем багажника.
struct Car : Hashable{
    var model : String{
        didSet{
            print("Модель машины изменена на: \(model)")
        }
    }
    var dateManufacture : Int{
        didSet{
            print("Год производства данного автомобиля изменен на: \(dateManufacture)")
        }
    }
    var isEngineRunning : Engine{
        didSet{
            print("Двигатель был \(isEngineRunning.rawValue)")
        }
    }
    var isWindowOpen : Window{
        didSet{
            print("Окна стали \(isWindowOpen.rawValue)")
        }
    }
    var volumeMax : Int{
        didSet{
            print("Максимальный объем багажника изменен на: \(volumeMax)")
        }
    }
    var volumeCurrent : Int{
        didSet{
            print("В багажнике стало свободно: \(volumeCurrent)")
        }
    }
    var volumeStorage : VolumeStorage{
        get{
            if volumeMax == volumeCurrent{
                return .empty
            }
            else {
                return .fill
            }
        }
    }
    let typeOfStorage : TypeOfStorage = .trunk
    init(model: String, dateManufacture: Int, isEngineRunning: Engine, isWindowOpen: Window, volumeMax: Int, volumeCurrent: Int) {
        self.model = model
        self.dateManufacture = dateManufacture
        self.isEngineRunning = isEngineRunning
        self.isWindowOpen = isWindowOpen
        self.volumeMax = volumeMax
        self.volumeCurrent = volumeCurrent
        printAll()
    }
    
    //Добавить в структуры метод с одним аргументом типа перечисления, который будет менять свойства структуры в зависимости от действия.
    mutating func toggleThing(_ thing : Toggle){
        switch thing{
        case Engine.engineOn:
            isEngineRunning = .engineOn
        case Engine.engineOff:
            isEngineRunning = .engineOff
        case Window.open:
            isWindowOpen = .open
        case Window.close:
            isWindowOpen = .close
        default:
            break
        }
    }
    
    func printAll(){
        print("Модель автомобиля \(model)")
        print("Дата производства: \(dateManufacture)")
        print("Двигатель \(isEngineRunning.rawValue)")
        print("Окна \(isWindowOpen.rawValue)")
        print("Максимальный объем багажника: \(volumeMax)")
        print("При этом свободного места: \(volumeCurrent)")
        print("Поэтому в багажнике \(volumeStorage.rawValue)")
    }
}


//сделал на всякий, написано описать несколько стурктур, но вдруг надо было только одну
struct AutoTruck{
    
    var model : String{
        didSet{
            print("Модель машины изменена на: \(model)")
        }
    }
    var dateManufacture : Int{
        didSet{
            print("Год производства данного автомобиля изменен на: \(dateManufacture)")
        }
    }
    var isEngineRunning : Engine{
        didSet{
            print("Двигатель был \(isEngineRunning.rawValue)")
        }
    }
    var isWindowOpen : Window{
        didSet{
            print("Окна стали \(isWindowOpen.rawValue)")
        }
    }
    var volumeMax : Int{
        didSet{
            print("Максимальный объем багажника изменен на: \(volumeMax)")
        }
    }
    var volumeCurrent : Int{
        didSet{
            print("В багажнике стало свободно: \(volumeCurrent)")
        }
    }
    var volumeStorage : VolumeStorage{
        get{
            if volumeMax == volumeCurrent{
                return .empty
            }
            else {
                return .fill
            }
        }
    }
    let typeOfStorage : TypeOfStorage = .carCase
    init(model: String, dateManufacture: Int, isEngineRunning: Engine, isWindowOpen: Window, volumeMax: Int, volumeCurrent: Int) {
        self.model = model
        self.dateManufacture = dateManufacture
        self.isEngineRunning = isEngineRunning
        self.isWindowOpen = isWindowOpen
        self.volumeMax = volumeMax
        self.volumeCurrent = volumeCurrent
        printAll()
    }
    
    //Добавить в структуры метод с одним аргументом типа перечисления, который будет менять свойства структуры в зависимости от действия.
    mutating func toggleThing(_ thing : Toggle){
        switch thing{
        case Engine.engineOn:
            isEngineRunning = .engineOn
        case Engine.engineOff:
            isEngineRunning = .engineOff
        case Window.open:
            isWindowOpen = .open
        case Window.close:
            isWindowOpen = .close
        default:
            break
        }
    }
    
    func printAll(){
        print("Модель автомобиля \(model)")
        print("Дата производства: \(dateManufacture)")
        print("Двигатель \(isEngineRunning.rawValue)")
        print("Окна \(isWindowOpen.rawValue)")
        print("Максимальный объем багажника: \(volumeMax)")
        print("При этом свободного места: \(volumeCurrent)")
        print("Поэтому в багажнике \(volumeStorage.rawValue)")
    }
}

//Описать перечисление с возможными действиями с автомобилем: запустить/заглушить двигатель, открыть/закрыть окна, погрузить/выгрузить из кузова/багажника груз определенного объема.
//Инициализировать несколько экземпляров структур. Применить к ним различные действия.
var showOutput = false
if showOutput{
    var simpleCar = Car(model: "b", dateManufacture: 12, isEngineRunning: .engineOn, isWindowOpen: .open, volumeMax: 250, volumeCurrent: 250)
    print()
    simpleCar.toggleThing(Engine.engineOn)
    simpleCar.toggleThing(Window.open)
    simpleCar.isWindowOpen.openCloseWindow()
    simpleCar.isEngineRunning.startStopEngine()
    print()
    simpleCar.model = "BMW"
    simpleCar.dateManufacture = 2019
    print()
    simpleCar.printAll()
}
print()



var bigCar = Car(model: "Камаз", dateManufacture: 2016, isEngineRunning: .engineOff, isWindowOpen: .close, volumeMax: 20000, volumeCurrent: 10000)
print("\n\n")
var smallCar = Car(model: "BMW", dateManufacture: 1999, isEngineRunning: .engineOn, isWindowOpen: .close, volumeMax: 250, volumeCurrent: 250)




//Положить объекты структур в словарь как ключи! а их названия как строки например
var dict : [Car : String] = [smallCar : smallCar.model]
dict[bigCar] = bigCar.model

print("\n\n\n\n\n")


       
//Почитать о Capture List (см ссылку ниже) - и описать своими словами и сделать скрин своего примера и объяснени           //https://www.hackingwithswift.com/articles/179/capture..
/*Капец как лень писать код
Я просто скажу, что можно сделать вьюху, сильной ссылкой цепануть табличку, из таблички сильной ссылкой цепануть какой-нибудь класс, который будет писать в эту табличку, все остальные ссылки слабые
Класс цепляем к табличке слабой ссылкой, иначе, когда все будет разваливаться - получим цикл сильных ссылок
 
Я бы описал правильный процесс создания сильных ссылкой пирамидальной схемой
 
*******
 *****
  ***
   *

Когда мы убираем нижний элемент, сборщик мусора должен вычистить все остальное

Что-же насчет замыканий, ну надо бы разобрать еще пару примеров и следить за их захватом ссылок
Такие дела
А ведь я еще и диплом пишу сейчас)))
там крч генератор комнаты для тестирования алгоритмов управления роботов пылесосов)
*/





//Набрать код который на белом скрине понять в чем там проблема и решить эту проблему
class NewCar {
    var driver : Man?
    
    deinit {
        print("Car was deleted from memory")
    }
}

class Man {
    weak var myCar : NewCar?
    //var myCar : NewCar? вот тут проблема
//Не каждый человек имеет машину, но каждая машина имеет человека
    deinit {
        print("Man was deleted from memory")
    }
}

var car : NewCar? = NewCar()

var man : Man? = Man()

car?.driver = man

man?.myCar = car

man = nil
car = nil

print("\n\n\n\n\n\n")
//У нас есть класс мужчины и его паспорта. Мужчина может родиться и не иметь паспорта, но паспорт выдается конкретному мужчине и не может выдаваться без указания владельца. Чтобыразрешить эту проблему, ссылку на паспорт у мужчины сделаем обычной опциональной, а ссылку на владельца у паспорта – константой. Также добавим паспорту конструктор, чтобы сразу определить его владельца. Таким образом, человек сможет существовать без паспорта, сможет его поменять или выкинуть, но паспорт может быть создан только с конкретным владельцем и никогда не может его сменить. Повторить все что на черном скрине и решить проблему соблюдая все правила!

class Human{
    var pasport : Passport?
    deinit {
        print("Man was deleted from memory")
    }
}

class Passport{
    unowned var human : Human
    
    init(human: Human) {
        self.human = human
    }
    
    deinit {
        print("Pasport was deleted from memory")
    }
}

var human : Human? = Human()

var passport : Passport? = Passport(human: human!)

human?.pasport = passport

passport = nil
human = nil


